<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateSatuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satuans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('satuan');
            $table->timestamps();
        });

        DB::table('satuans')->insert(
            array(
                'satuan' => 'Meter',
            )
        );

        DB::table('satuans')->insert(
            array(
                'satuan' => 'Kilogram',
            )
        );

        DB::table('satuans')->insert(
            array(
                'satuan' => 'Buah',
            )
        );

        DB::table('satuans')->insert(
            array(
                'satuan' => 'Kotak',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satuans');
    }
}
